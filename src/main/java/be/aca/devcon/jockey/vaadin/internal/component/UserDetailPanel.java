package be.aca.devcon.jockey.vaadin.internal.component;

import be.aca.devcon.jockey.service.api.Jockey;
import com.vaadin.server.ExternalResource;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

public class UserDetailPanel extends VerticalLayout {

	private Label titleLabel;
	private Image profileImage;
	private Label idValue;
	private Label horseValue;

	public UserDetailPanel() {
		titleLabel = new Label();
		profileImage = new Image();
		Label idLabel = new Label("ID");
		idValue = new Label();
		Label horseLabel = new Label("Horse");
		horseValue = new Label();

		VerticalLayout idLayout = new VerticalLayout();
		idLayout.addComponent(idLabel);
		idLayout.addComponent(idValue);

		VerticalLayout horseLayout = new VerticalLayout();
		horseLayout.addComponent(horseLabel);
		horseLayout.addComponent(horseValue);

		VerticalLayout description = new VerticalLayout();
		description.addComponent(idLayout);
		description.addComponent(horseLayout);
		description.setSpacing(true);

		HorizontalLayout hl = new HorizontalLayout();
		hl.addComponent(profileImage);
		hl.addComponent(description);

		addComponent(titleLabel);
		addComponent(hl);

		BackToOverviewLink backToOverviewLink = new BackToOverviewLink();
		backToOverviewLink.addClickListener(event -> setVisible(false));
		addComponent(backToOverviewLink);

		setVisible(false);
	}

	public void setJockey(Jockey jockey) {
		titleLabel.setValue(jockey.getName());
		profileImage.setSource(new ExternalResource(jockey.getPortraitUrl()));
		idValue.setValue(String.valueOf(jockey.getId()));
		horseValue.setValue(jockey.getHorse());
	}
}
