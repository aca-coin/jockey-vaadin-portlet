package be.aca.devcon.jockey.vaadin.internal.component;

import com.vaadin.ui.Button;

public class SearchButton extends Button {

	public SearchButton() {
		setCaption("Search");
	}
}
