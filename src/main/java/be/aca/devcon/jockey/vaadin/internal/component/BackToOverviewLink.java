package be.aca.devcon.jockey.vaadin.internal.component;

import com.vaadin.ui.Button;

public class BackToOverviewLink extends Button {

	public BackToOverviewLink() {
		setStyleName("link");
		setCaption("Back to overview");
	}
}
