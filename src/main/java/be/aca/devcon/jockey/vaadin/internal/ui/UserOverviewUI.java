package be.aca.devcon.jockey.vaadin.internal.ui;

import be.aca.devcon.jockey.service.api.Jockey;
import be.aca.devcon.jockey.service.api.JockeyService;
import be.aca.devcon.jockey.vaadin.internal.component.*;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ServiceScope;

import java.util.List;

@Component(
		scope = ServiceScope.PROTOTYPE,
		property = {
			"com.liferay.portlet.display-category=ACA Devcon",
			"com.liferay.portlet.instanceable=true",
			"com.liferay.portlet.css-class-wrapper=jockey-vaadin-portlet",
			"javax.portlet.display-name=Jockey Vaadin Portlet",
			"javax.portlet.security-role-ref=power-user,user",
			"javax.portlet.init-param.vaadin.resources.path=/o/vaadin7.7.0"
		},
		service = UI.class
)
public class UserOverviewUI extends UI {

	@Reference private JockeyService jockeyService;

	private TeamDropdown teamDropdown;
	private SearchButton searchButton;
	private UserOverviewGrid userOverviewGrid;

	private UserDetailPanel userDetailPanel;

	protected void init(VaadinRequest request) {
		List<Jockey> jockeys = jockeyService.getJockeys(null);
		List<String> teams = jockeyService.getTeams();

		UserDetailPanel userDetailPanel = new UserDetailPanel();
		UserOverviewGrid userOverviewGrid = new UserOverviewGrid(jockeys, userDetailPanel);
		TeamDropdown teamDropdown = new TeamDropdown(teams);
		SearchButton searchButton = new SearchButton();
		searchButton.addClickListener(event -> userOverviewGrid.populateData(jockeyService.getJockeys(teamDropdown.getValue())));

		HorizontalLayout searchFilter = new HorizontalLayout();
		searchFilter.addComponent(teamDropdown);
		searchFilter.addComponent(searchButton);
		searchFilter.setSpacing(true);

		VerticalLayout verticalLayout = new VerticalLayout();
		verticalLayout.addComponent(searchFilter);
		verticalLayout.addComponent(userOverviewGrid);
		verticalLayout.setSpacing(true);

		setTheme("valo");
		getPage().getStyles().add(".jockey-vaadin-portlet th {height: 37px !important} .jockey-vaadin-portlet th div {line-height: 25px; font-weight: bold} .valo.v-app, .valo.v-app-loading {background: white !important}");

		setContent(verticalLayout);
	}
}