package be.aca.devcon.jockey.vaadin.internal.portlet;

import com.vaadin.server.DefaultUIProvider;
import com.vaadin.server.UIProvider;
import com.vaadin.server.VaadinPortlet;
import com.vaadin.server.VaadinSession;

import javax.portlet.PortletException;
import java.util.ArrayList;
import java.util.List;

public class VaadinOSGiPortlet extends VaadinPortlet {

	private UIProvider provider;

	public VaadinOSGiPortlet(UIProvider provider) {
		this.provider = provider;
	}

	protected void portletInitialized() throws PortletException {
		getService().addSessionInitListener(event -> {
			VaadinSession session = event.getSession();
			List<UIProvider> uiProviders = new ArrayList<>(session.getUIProviders());

			uiProviders.stream().filter(provider -> DefaultUIProvider.class.getCanonicalName().equals(provider.getClass().getCanonicalName())).forEach(session::removeUIProvider);

			session.addUIProvider(provider);
		});
	}

}
