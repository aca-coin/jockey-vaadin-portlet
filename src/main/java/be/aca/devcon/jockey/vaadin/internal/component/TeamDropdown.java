package be.aca.devcon.jockey.vaadin.internal.component;

import com.vaadin.ui.ComboBox;

import java.util.List;

public class TeamDropdown extends ComboBox {

	private static final String NULL_ITEM = "Select team";

	public TeamDropdown(List<String> teams) {
		addItem(NULL_ITEM);
		addItems(teams);

		setNullSelectionItemId(NULL_ITEM);
	}

	public String getValue() {
		return (String) super.getValue();
	}
}




