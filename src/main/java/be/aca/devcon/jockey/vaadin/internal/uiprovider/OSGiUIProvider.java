package be.aca.devcon.jockey.vaadin.internal.uiprovider;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.vaadin.server.UIClassSelectionEvent;
import com.vaadin.server.UICreateEvent;
import com.vaadin.server.UIProvider;
import com.vaadin.ui.UI;
import org.osgi.framework.ServiceObjects;
import org.osgi.framework.ServiceReference;

import java.util.HashMap;
import java.util.Map;

public class OSGiUIProvider extends UIProvider {

	private Log log = LogFactoryUtil.getLog(OSGiUIProvider.class);
	private ServiceObjects<UI> serviceObjects;
	private Class<UI> uiClass;
	private UI ui;

	public OSGiUIProvider(ServiceObjects<UI> serviceObjects) {
		super();

		this.serviceObjects = serviceObjects;
		ui = serviceObjects.getService();
		uiClass = (Class<UI>) ui.getClass();
	}

	public Class<? extends UI> getUIClass(UIClassSelectionEvent event) {
		return uiClass;
	}

	public UI createInstance(UICreateEvent uiCreateEvent) {
		final ServiceObjects<UI> serviceObjects = this.serviceObjects;
		final UI ui = serviceObjects.getService();

		ui.addDetachListener(event -> {
			serviceObjects.ungetService(ui);

			if (log.isDebugEnabled()) {
				log.debug("unregistered UI " + ui.toString());
			}
		});
		return ui;
	}

	public UI getDefaultUI() {
		return ui;
	}

	public Map<String, Object> getProperties() {
		Map<String, Object> properties = new HashMap<>(); ServiceReference<UI> serviceReference = serviceObjects.getServiceReference();
		for (String key : serviceReference.getPropertyKeys()) {
			properties.put(key, serviceReference.getProperty(key));
		}
		return properties;
	}
}
