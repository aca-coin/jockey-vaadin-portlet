package be.aca.devcon.jockey.vaadin.internal.component;

import be.aca.devcon.jockey.service.api.Jockey;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.GeneratedPropertyContainer;
import com.vaadin.data.util.PropertyValueGenerator;
import com.vaadin.shared.ui.grid.HeightMode;
import com.vaadin.ui.Grid;
import com.vaadin.ui.renderers.ButtonRenderer;

import java.util.List;

public class UserOverviewGrid extends Grid {

	private UserDetailPanel userDetailPanel;
	private BeanItemContainer<Jockey> container;

	public UserOverviewGrid(List<Jockey> jockeys, UserDetailPanel userDetailPanel) {
		this.userDetailPanel = userDetailPanel;

		populateData(jockeys);

		removeAllColumns();
		addColumn("id");
		addColumn("name");
		addColumn("horse");
		addColumn("teamName");
//		addViewColumn();

		setWidth(100, Unit.PERCENTAGE);
		setHeightMode(HeightMode.ROW);
	}

	public void populateData(List<Jockey> jockeys) {
		container = new BeanItemContainer<>(Jockey.class, jockeys);
		setContainerDataSource(container);

		if (!jockeys.isEmpty()) {
			setHeightByRows(jockeys.size());
		}
	}

	private void addViewColumn() {
		GeneratedPropertyContainer gpc = new GeneratedPropertyContainer(container);
		gpc.addGeneratedProperty("view", new PropertyValueGenerator<String>() {
			public String getValue(Item item, Object itemId, Object propertyId) {
				return "View";
			}
			public Class<String> getType() {
				return String.class;
			}
		});

		addColumn("view");
		getColumn("view").setRenderer(new ButtonRenderer(event -> {
			userDetailPanel.setJockey((Jockey) event.getItemId());
			userDetailPanel.setVisible(true);
		}));
	}
}
