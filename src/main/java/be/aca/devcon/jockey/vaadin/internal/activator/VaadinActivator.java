package be.aca.devcon.jockey.vaadin.internal.activator;

import be.aca.devcon.jockey.vaadin.internal.portlet.VaadinOSGiPortlet;
import be.aca.devcon.jockey.vaadin.internal.uiprovider.OSGiUIProvider;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.vaadin.ui.UI;
import org.osgi.framework.*;
import org.osgi.util.tracker.ServiceTracker;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

import javax.portlet.Portlet;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

public class VaadinActivator implements BundleActivator, ServiceTrackerCustomizer<UI, UI> {

	private Log log = LogFactoryUtil.getLog(VaadinActivator.class);
	private ServiceTracker<UI, UI> serviceTracker;
	private Map<ServiceReference<UI>, ServiceRegistration<Portlet>> portletServiceRegistration;

	public void start(BundleContext bundleContext) throws Exception {
		portletServiceRegistration = new HashMap<>();
		serviceTracker = new ServiceTracker<>(bundleContext, UI.class, this);
		serviceTracker.open();

		if (log.isDebugEnabled()) {
			log.debug("started");
		}
	}

	public void stop(BundleContext bundleContext) throws Exception {
		portletServiceRegistration.values().forEach(ServiceRegistration::unregister);

		portletServiceRegistration.clear();
		portletServiceRegistration = null;

		if (serviceTracker != null) {
			serviceTracker.close();
			serviceTracker = null;
		}

		if (log.isDebugEnabled()) {
			log.debug("stopped");
		}
	}

	@Override
	public UI addingService(ServiceReference<UI> uiServiceReference) {
		Bundle bundle = FrameworkUtil.getBundle(getClass());
		BundleContext bundleContext = bundle.getBundleContext();

		ServiceObjects<UI> serviceObjects = bundleContext.getServiceObjects(uiServiceReference);
		OSGiUIProvider osgiUIProvider = new OSGiUIProvider(serviceObjects);

		Dictionary<String, Object> properties = new Hashtable<>();

		for (Map.Entry<String, Object> entry : osgiUIProvider.getProperties().entrySet()) {
			String key = entry.getKey();
			Object defaultValue = entry.getValue();

			Object value = uiServiceReference.getProperty(key);
			if (value != null) {
				properties.put(key, value);
			} else if (defaultValue != null) {
				properties.put(key, defaultValue);
			}
		}

		ServiceRegistration<Portlet> serviceRegistration = bundleContext.registerService(Portlet.class, new VaadinOSGiPortlet(osgiUIProvider), properties);
		portletServiceRegistration.put(uiServiceReference, serviceRegistration);

		if (log.isDebugEnabled()) {
			log.debug("portlet created for " + osgiUIProvider.getUIClass(null).getName());
		}

		return osgiUIProvider.getDefaultUI();
	}

	public void modifiedService(ServiceReference<UI> serviceReference, UI ui) {
		// Ignore
	}

	public void removedService(ServiceReference<UI> uiServiceReference, UI ui) {
		if (portletServiceRegistration != null) {
			ServiceRegistration<Portlet> serviceRegistration = portletServiceRegistration.get(uiServiceReference);
			portletServiceRegistration.remove(uiServiceReference);

			serviceRegistration.unregister();

			if (log.isDebugEnabled()) {
				log.debug("portlet unregistered for " + ui.getClass().getName());
			}
		}
	}
}